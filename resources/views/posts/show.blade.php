@extends('layouts.app')


@section('content')
	<div class="card col-6 mx-auto">
		<div class="card-body">
		<h2 class="card-title">{{$post->title}}</h2>
		<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
		<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
		<p class="card-subtitle text-muted mb-3">Updated at: {{$post->updated_at}}</p>
		<h4>Content:</h4>
		<p class="card-text">{{$post->body}}</p>
		<p class="mt-4">Likes: {{$post->likes->count()}} | Comments: {{$post->comments->count()}}</p>

		
		@if(Auth::user())
		@if(Auth::id() != $post->user_id)
			<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
				@method('PUT')
				@csrf
				@if($post->likes->contains('user_id', Auth::id()))
				<button class="btn btn-danger">Unlike</button>
				@else
					<button class="btn btn-success">Like</button>

				@endif
			</form>
		@endif
		@endif




		@if(Auth::user())
		    @if(Auth::id() != $post->user_id)
		        <button id="show-comment-modal" class="btn btn-primary">Comment</button>
		    @endif
		@endif

		<!-- Add the modal HTML structure -->
		<div class="modal" tabindex="-1" id="comment-modal">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h5 class="modal-title">Comment on Post</h5>
		                <button type="button" class="btn-close" id="close-comment-modal" aria-label="Close"></button>
		            </div>
		            <div class="modal-body">
		                <form method="POST" action="/posts/{{$post->id}}/comment" id="comment-form">
		                    @csrf

		                    <textarea class = "form-control" id ="content" name = "content"></textarea>
		                </form>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-secondary" id="close-modal-button" data-bs-dismiss="modal">Close</button>
		                <button type="submit" form="comment-form" class="btn btn-primary">Comment</button>
		            </div>
		        </div>
		    </div>
		</div>

		<script>
		    // JavaScript to handle the modal functionality
		    var commentModal = document.getElementById("comment-modal");
		    var showCommentModalButton = document.getElementById("show-comment-modal");
		    var closeCommentModalButton = document.getElementById("close-comment-modal");
		    var closeModalButton = document.getElementById("close-modal-button");

		    // Show the modal when the "Comment" button is clicked
		    showCommentModalButton.onclick = function() {
		        commentModal.style.display = "block";
		    }

		    // Close the modal when the close button (x) or close modal button is clicked
		    closeCommentModalButton.onclick = function() {
		        commentModal.style.display = "none";
		    }

		    closeModalButton.onclick = function() {
		        commentModal.style.display = "none";
		    }

		    // Close the modal if the user clicks outside of the modal content
		    window.onclick = function(event) {
		        if (event.target == commentModal) {
		            commentModal.style.display = "none";
		        }
		    }
		</script>



		</br>
		<a href="/posts" class="card-link btn btn-info mt-2">View all posts</a>
		</div>
	</div>


	<!-- Comments section -->
		<div class="mt-4">
		    <h4>Comments:</h4>
		        @foreach($post->comments as $comment)
		            <div class="card mb-3 p-3">
		                <h1 class="card-text text-center">{{$comment->body}}</h1>
		                <div class="card-body ms-auto">
		                    <p class="card-subtitle text-muted">Commented by: {{$comment->user->name}}</p>
		                    <p class="card-subtitle text-muted">Created at: {{$comment->user->created_at}}</p>
		                </div>
		            </div>
		        @endforeach
		</div>








@endsection