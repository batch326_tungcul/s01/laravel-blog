@extends('layouts.app')

@section('content')
   <!--  <form class = "col-10 p-5 mx-auto" method = "POST" action="{{ route('post.update', ['id' => $post->id]) }}">
        
        
        @csrf
        @method('PUT')
        <div class = "form-group">

            <label for = 'title'>Title:</label>
            <input type="text" name="title" class="form-control" id = "title" value="{{ $post->title }}"/>
            
        </div>

        <div class = "form-group">
            <label for = "Content">Content:</label>
            <textarea class = "form-control" id ="content" name = "content" rows = 3>{{ $post->content }}</textarea>
            
        </div>

        <div class = "mt-2">
            <button class="btn btn-primary">Update Post</button>
        </div>



    </form> -->
   
        <form class="col-10" action="{{ route('post.update', ['id' => $post->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class = "form-group">
            <h5>Title:</h5>
            <input type="text" name="title" value="{{ $post->title }}" class="form-control">
            </div>

            <div class = "form-group">
            <h5>Content:</h5>
            <textarea name="content" class = "form-control">{{ $post->content }}</textarea>
            </div>

            <div class = "mt-2">
            <button type="submit" class="btn btn-primary">Update Post</button>
            </div>
        </form>
@endsection
