@extends('layouts.app')

@section('content')
	<form class = "col-3 bg-secondary p-5 mx-auto" method = "POST" action = '/posts'>
		<!-- CSRF stands for cross-site request Forgery. It is form of attack where malicious users may send malicious request while pretending to be authorize user. Laravel uses token to detect if form input request have not been tempered with. -->
		
		@csrf
		<div class = "form-group">

			<label for = 'title'>Title:</label>
			<input type="text" name="title" class="form-control" id = "title"/>
			
		</div>

		<div class = "form-group">
			<label for = "Content">Content:</label>
			<textarea class = "form-control" id ="content" name = "content" rows = 3></textarea>
			
		</div>

		<div class = "mt-2">
			<button class="btn btn-primary">Create Post</button>
		</div>



	</form>
@endsection