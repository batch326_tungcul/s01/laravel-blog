<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Models\Post;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $post = Post::all();
    return view('welcome')->with('posts', $post);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);

// This route for creation of a new post:
Route::get('/posts/create', [PostController::class, 'createPost']);


// This route for saving the post on our database:
Route::post('/posts', [PostController::class, 'savePost']);

// This Route for is for the list of post on our database:
Route::get('/posts', [PostController::class, 'showPosts']);

// Define a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [PostController::class, 'myPosts']);

// Define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show'])->name('post.show');


// Edit post and Update post

Route::get('/posts/{id}/edit', [PostController::class, 'edit'])->name('post.edit');

Route::put('/posts/{id}', [PostController::class, 'update'])->name('post.update');

// Route to archive a post
Route::delete('/posts/{id}', [PostController::class, 'archive'])->name('post.archive');

// Define a web route that will call the function for liking and unliking a specific post:
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// Comment a post
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);











