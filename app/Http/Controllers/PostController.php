<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // controller for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();

        return redirect('/login');
    }

    // Action to return a view containing the form for a blog post creation

    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        // to check whether ther is an authenticated user or none:
        if(Auth::user()){
            //intantiate a new Post Object fromt the Post Method and then save it in $post variable:
            $post = new Post;

            //define the properties of the $post object using the reveive form data
            $post->title=$request->input('title');
            $post->body=$request->input('content');

            // this will get the id of the authenticated user and set it as a foreign key  user_id of the new post.
            $post->user_id=(Auth::user()->id);

            // save the post object in our Post Table:
            $post->save();

            return redirect('/posts/create');
        }
        else{
            return redirect('/login');
        }
    }

    // this controller will return all the blog posts:
     public function showPosts(){
        // this all() method will save all the records in out Post Table in our $post variable
        $posts = Post::all();

        return view('posts.showPosts')->with('posts', $posts);
     }

     // action for showing only the post authored by the authenticated user
     public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;
            return view('posts.showPosts')->with('posts', $posts);
        }else {
            return redirect('/login');
        }
     }
     // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
     public function show($id){

        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
     }



    // action to edit and update post

     public function edit($id){
     
         $post = Post::findOrFail($id);
         return view('posts.edit', compact('post'));
     }

     public function update(Request $request, $id){
        $post = Post::find($id);
        $post->title=$request->input('title');
        $post->body=$request->input('content');
        $post->save();
        return redirect()->route('post.show', ['id' => $post->id]);
     }

     public function archive($id){
             $post = Post::findOrFail($id);
             
             // Set isActive to false
             $post->isActive = false;
             $post->save();
             
             return redirect()->route('post.show', ['id' => $post->id]);
    }

    public function like($id){
        $post = Post::find($id);

        if($post->user_id != Auth::user()->id){

            if($post->likes->contains("user_id", Auth::user()->id)){
                    PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
            }else{
                // create a new like record to like thist post
                // intantiate a new PostLike object from the PostLike Model
                $postlike = new PostLike;
                // define the properties of the $postlike
                $postlike->post_id = $post->id;
                $postlike->user_id = Auth::user()->id;

                // save this postlike object in the database
                $postlike->save();

            }
        }

        return redirect("/posts/$id");
    }
     
    public function comment(Request $request, $id)
    {
        // Validate the comment input
        $request->validate([
            'content' => 'required',
        ]);

        // Create a new comment
        $postComment = new PostComment;
        $postComment->post_id = $id;
        $postComment->user_id = Auth::user()->id;
        $postComment->body = $request->input('content');
        $postComment->save();

        return redirect("/posts/$id");
    }


}

















